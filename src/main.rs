// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
extern crate log4rs;
extern crate iron;
extern crate rustache;
extern crate num;
extern crate rustc_serialize;
extern crate chrono;
extern crate rand;
extern crate url;

#[macro_use]
pub mod utils;
pub mod configuration;

mod server;

use std::sync::Arc;
use std::path::Path;
use configuration::Configuration;
use configuration::JsonConfiguration;
use server::Server;
use utils::time::SystemTimeSource;
use utils::time::TimeSource;

fn main() {
	setup_log();
	let config = setup_config();
	let time: Arc<TimeSource> = Arc::new(SystemTimeSource);
	info!("Starting OpenID server...");
	Server::create(&config, &time)
		.map_err(|e| error!("Fatal during startup: {}", e))
		.unwrap();
}

fn setup_log() {
	log4rs::init_file("config/log4rs.json", Default::default()).unwrap();
}

fn setup_config() -> Arc<Configuration> {
	let config = JsonConfiguration::from_file(Path::new("config/app.json"));
	let result = config.unwrap();
	return Arc::new(result);
}