
// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

///
/// An Option<> support utility
///

///
/// A convenient macro for Option-to-Result mapping.
///
/// This macro is identical to a simple ok_or invocation with format! inside.
/// Since parenthees overflow lowers maintainability, we provide this helper.
///
/// # Example
///
/// fn load_value(key: &str) -> Option<String> { ... }
/// fn erroneous_func(key: &str) -> Result<String, String> {
/// 	let value = try_opt!(load_value(key), "No key {}", key);
/// 	return Ok(value):
/// }
///
/// In try_opt! invocation, if Option::None would be returned, erroneous_func
/// will return with Result::Err("No key " + key).
///
#[macro_export]
macro_rules! try_opt {
	( $expr:expr, $fmt:expr, $( $arg:expr ), *) => {
		try!($expr.ok_or_else(|| format!($fmt, $( $arg )*)))
	};
	( $expr:expr, $fmt:expr ) => {
		try!($expr.ok_or_else(|| $fmt));
	};
}

macro_rules! try_or {
	( $expr:expr, $err:expr ) => {
		try!($expr.ok_or_else(|| $err))
	};
}

macro_rules! opt {
	( $expr:expr ) => {
		match $expr {
			None => return None,
			Some(x) => x,
		}
	};
}