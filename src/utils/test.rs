
macro_rules! assert_float_eq {
	( $left:expr, $right:expr, $epsilon:expr ) => {
		let left_f = f64::from($left);
		let right_f = f64::from($right);
		let diff = left_f - right_f;
		assert!(diff.abs() < $epsilon)
	};
	( $left:expr, $right:expr ) => {
		assert_float_eq!( $left, $right, 0.000001_f64 )
	};
}