
// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

///
/// I really want to use library functions for that, but Rand has
/// quite crappy support for dynamic dispatch. Since I don't want
/// to hang around covered in trait bounds and generics, I'd rather
/// implement this stuff on my own.
///
/// Such a range generation does not provide good distribution, but
/// I can live with this. Rust has the long way to go before it can
/// be used in serious projects.
///
/// random_string is a custom function. File users are advised to use
/// special functions instead, like random_alnum
///

use rand::Rng;

const ALNUM_RANGE: &'static str = "qwertyuiopasdfghjklzxcvbnm0987654321";
const ALNUM_CS_RANGE: &'static str = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0987654321";
const LETTERS_RANGE: &'static str = "qwertyuiopasdfghjklzxcvbnm";
const LETTERS_CS_RANGE: &'static str = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";

pub fn random_alnum_case_insensitive(rng: &mut Rng, size: usize) -> String {
	return random_string(rng, size, ALNUM_RANGE);
}

pub fn random_alnum_case_sensitive(rng: &mut Rng, size: usize) -> String {
	return random_string(rng, size, ALNUM_CS_RANGE);
}

pub fn random_letters_case_insensitive(rng: &mut Rng, size: usize) -> String {
	return random_string(rng, size, LETTERS_RANGE);
}

pub fn random_letters_case_sensitive(rng: &mut Rng, size: usize) -> String {
	return random_string(rng, size, LETTERS_CS_RANGE);
}

pub fn random_string(rng: &mut Rng, size: usize, alphabet: &str) -> String {
	let mut result = String::with_capacity(size);
	for _ in 0..size {
		let index = rng.next_u32() as usize % alphabet.len();
		let letter = alphabet.chars().nth(index);
		result.push(letter.unwrap());
	}
	return result;
}

pub fn random_bytes(rng: &mut Rng, size: usize) -> Vec<u8> {
	let mut result = vec![0; size];
	rng.fill_bytes(&mut result[..]);
	return result;
}