
// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate mount;
extern crate staticfile;

use std::sync::Arc;
use std::ops::Deref;
use std::time::Duration;
use iron::middleware::Handler;
use iron::{Request, Response, IronResult};
use configuration::Configuration;
use self::staticfile::Static;
use self::mount::Mount;

const CACHE_EXPIRATION_PARAM: &'static str = "static.cache.expires.seconds";
lazy_static! {
	static ref DEFAULT_CACHE_EXPIRATION: Duration = Duration::from_secs(60 * 60);
}

pub struct StaticHandler(Arc<Configuration>, Mount);
impl StaticHandler {

	pub fn new(config: Arc<Configuration>) -> StaticHandler
	{
		let handlers = load_handlers(config.deref());
		let mount = build_mount(&handlers, config.deref());
		return StaticHandler(config, mount);
	}

	fn mount(&self) -> &Mount {
		let &StaticHandler(_, ref mount) = self;
		return mount;
	}
}
impl Handler for StaticHandler {
	fn handle(&self, req: &mut Request) -> IronResult<Response> {
		return self.mount().handle(req);
	}
}

fn load_handlers(config: &Configuration) -> Vec<(String, String)> {
	let loaded = config.load_map_str("static.mounts");
	if let Some(mounts) = loaded {
		return mounts.iter()
			.inspect(|&(k, v)| info!("Static handler loaded: URL prefix is '{}', path prefix is {}'", k, v))
			.map(|(k, v)| (k.clone(), v.clone()))
			.collect();
	} else {
		return Vec::new();
	}
}

fn build_mount(params: &Vec<(String, String)>, config: &Configuration) -> Mount {
	let cache_duration = config.load_duration_secs(CACHE_EXPIRATION_PARAM)
		.unwrap_or_else(|| DEFAULT_CACHE_EXPIRATION.clone());
	let mut handler = Mount::new();
	for &(ref url_prefix, ref fs_prefix) in params.iter() {
		use std::path::Path;
		let path_handler = Static::new(Path::new(fs_prefix));
		let cached_path_handler = path_handler.cache(cache_duration.clone());
		handler.mount(url_prefix, cached_path_handler);
	}
	return handler;
}