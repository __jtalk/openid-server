// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate rustache;

pub mod welcome;
pub mod user;
pub mod openid;

use std::cmp::max;
use chrono::Datelike;
use utils::time::TimeSource;
use configuration::Configuration;
use self::rustache::HashBuilder;

pub fn load_base<'a>(mut builder: HashBuilder<'a>, config: &Configuration, time: &TimeSource) -> HashBuilder<'a> {
	builder = builder.insert_string("description_location", config.load_string_or("page.any.description", "https://jtalk.me/projects.xhtml?name=openid-server"));
	builder = builder.insert_string("minifier", config.load_string_or("page.any.resource.suffix", ".min"));
	builder = builder.insert_string("header", config.load_string_or("page.any.header", "OpenID Server"));
	builder = builder.insert_string("owner_url", config.load_string_or("page.any.owner_url", "https://jtalk.me/"));
	builder = builder.insert_string("current_year", copyright_year(config, time));
	return builder;
}

fn copyright_year(config: &Configuration, time: &TimeSource) -> String {
	let configured_year = config.load_int64("page.any.copyright.year").unwrap_or(0_i64);
	let current_year = time.now_utc().year() as i64;
	let chosen_year = max(current_year, configured_year).to_string();
	debug!("Copyright year loaded: current = '{}', configured = '{}', chosen = '{}'", current_year, configured_year, chosen_year);
	return chosen_year;
}