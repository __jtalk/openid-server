// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use super::fields::*;
use super::utils::*;

#[allow(non_camel_case_types)]
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Copy, Clone, Hash)]
pub enum OpenIdVersion {
	OpenID_2_0,
	OpenID_1_1,
	OpenID_1_0,
}

impl OpenIdVersion {

	pub fn from_ns(ns: &str) -> Option<OpenIdVersion> {
		use self::OpenIdVersion::*;
		let result = match ns {
			NS_VERSION_2_0 => Some(OpenID_2_0),
			NS_VERSION_1_1 => Some(OpenID_1_1),
			NS_VERSION_1_0 => Some(OpenID_1_0),
			_ => None,
		};
		return result;
	}

	pub fn ns_value(&self) -> &'static str {
		use self::OpenIdVersion::*;
		return match *self {
			OpenID_2_0 => NS_VERSION_2_0,
			OpenID_1_1 => NS_VERSION_1_1,
			OpenID_1_0 => NS_VERSION_1_0,
		};
	}
}

#[allow(non_camel_case_types)]
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Copy, Clone)]
pub enum OpenIdMode {
	Associate,
}

pub fn validate_version(request: &Body) -> Option<OpenIdVersion> {
	let found = opt!(extract_single(request, NS_VERSION_REQUEST));
	let result = OpenIdVersion::from_ns(found);
	if result.is_none() {
		warn!("Unknown OpenID version requested: {} is '{}'", NS_VERSION_REQUEST, found);
	}
	return result;
}

pub fn validate_mode(request: &Body) -> Option<OpenIdMode> {
	let found = opt!(extract_single(request, MODE));
	let result = match found.as_str() {
		MODE_ASSOCIATE => Some(OpenIdMode::Associate),
		other => {
			warn!("Unknown OpenID mode requested: {} is '{}'", MODE, other);
			None
		}
	};
	return result;
}