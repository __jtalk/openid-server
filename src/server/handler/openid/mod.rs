// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate urlencoded;

pub mod discovery;

mod validator;
mod error;
mod fields;
pub mod utils;
pub mod associate;

pub use self::fields::AUTH_URL_PREFIX;

use iron::Handler;
use iron::prelude::*;
use self::urlencoded::UrlEncodedBody;
use server::errors::body_parse::BodyParseError;
use self::validator::*;
use self::associate::*;
use self::error::OpenIdError;
use self::utils::OpenIdContext;

pub struct OpenIdHandler(OpenIdContext);
impl OpenIdHandler {

	pub fn new(ctx: OpenIdContext) -> OpenIdHandler {
		return OpenIdHandler(ctx);
	}
}
impl Handler for OpenIdHandler {
	fn handle(&self, req: &mut Request) -> IronResult<Response> {
		let body = try!(req.get_ref::<UrlEncodedBody>().map_err(|e| BodyParseError::from(e)));
		let version = try_or!(validate_version(body), OpenIdError::UnsupportedVersion);
		let mode = try_or!(validate_mode(body), OpenIdError::UnsupportedMode);
		let response = match mode {
			OpenIdMode::Associate => associate(&version, body, &self.0),
		};
		return response;
	}
}