// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::sync::RwLock;
use std::collections::HashMap;
use std::option::Option;
use chrono::DateTime;
use chrono::UTC;
use num::bigint::BigUint;
use super::data::*;
use super::super::validator::OpenIdVersion;

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Clone)]
pub struct AssociationMetadata {
	pub session_type: SessionType,
	pub association_type: AssociationType,
	pub protocol_version: OpenIdVersion,
	pub expires_at: DateTime<UTC>,
	pub mac_key: BigUint,
	pub session_key: Option<BigUint>,
}

pub trait Associations: Send + Sync {
	fn find(&self, handle: &str) -> Option<AssociationMetadata>;
	fn replace(&self, handle: &str, metadata: AssociationMetadata) -> Option<AssociationMetadata>;
	fn remove(&self, handle: &str) -> Option<AssociationMetadata>;
	fn clear(&self);
}

///
/// Read-write locks are slow. However, I cannot find any standard lock-free hash
/// map here. I can only find third-party solutions I cannot audit right now.
///
/// Since this service is not intended for high-load use, such an implementation seems
/// reasonable.
///
type InMemoryStorage = HashMap<String, AssociationMetadata>;
type InMemorySyncStorage = RwLock<InMemoryStorage>;

pub struct InMemoryAssociations(InMemorySyncStorage);
impl InMemoryAssociations {

	pub fn new() -> InMemoryAssociations {
		return InMemoryAssociations(RwLock::new(HashMap::new()));
	}

	fn storage(&self) -> &InMemorySyncStorage {
		return &self.0;
	}
}
impl Associations for InMemoryAssociations {

	fn find(&self, handle: &str) -> Option<AssociationMetadata> {
		let map = self.storage().read().unwrap();
		let found = map.get(handle);
		return found.map(|e| e.clone());
	}

	fn replace(&self, handle: &str, metadata: AssociationMetadata) -> Option<AssociationMetadata> {
		let mut map = self.storage().write().unwrap();
		let old = map.insert(handle.to_string(), metadata);
		return old;
	}

	fn remove(&self, handle: &str) -> Option<AssociationMetadata> {
		let mut map = self.storage().write().unwrap();
		let old = map.remove(handle);
		return old;
	}

	fn clear(&self) {
		let mut map = self.storage().write().unwrap();
		map.clear();
	}
}