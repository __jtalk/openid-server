// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use server::handler::openid::fields::*;

const ASSOCIATION_PLAIN_MAC_LENGTH: usize = 255;

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Copy, Clone, Hash)]
pub enum AssociationType {
	HmacSha1,
	HmacSha256,
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Copy, Clone, Hash)]
pub enum SessionType {
	Plain,
	DiffieHellmanSha1,
	DiffieHellmanSha256,
}

impl AssociationType {

	pub fn from_request(assoc_type: &str) -> Option<AssociationType> {
		use self::AssociationType::*;
		let result = match assoc_type {
			ASSOCIATION_TYPE_HMAC_SHA1 => Some(HmacSha1),
			ASSOCIATION_TYPE_HMAC_SHA256 => Some(HmacSha256),
			_ => None,
		};
		return result;
	}

	pub fn to_response(&self) -> &'static str {
		use self::AssociationType::*;
		return match *self {
			HmacSha1 => ASSOCIATION_TYPE_HMAC_SHA1,
			HmacSha256 => ASSOCIATION_TYPE_HMAC_SHA256,
		};
	}
}

impl SessionType {

	pub fn from_request(session_type: &str) -> Option<SessionType> {
		use self::SessionType::*;
		let result = match session_type {
			SESSION_TYPE_PLAIN => Some(Plain),
			SESSION_TYPE_DH_SHA1 => Some(DiffieHellmanSha1),
			SESSION_TYPE_DH_SHA256 => Some(DiffieHellmanSha256),
			_ => None,
		};
		return result;
	}

	pub fn to_response(&self) -> &'static str {
		use self::SessionType::*;
		return match *self {
			Plain => SESSION_TYPE_PLAIN,
			DiffieHellmanSha1 => SESSION_TYPE_DH_SHA1,
			DiffieHellmanSha256 => SESSION_TYPE_DH_SHA256,
		};
	}

	pub fn is_diffie_hellman(&self) -> bool {
		use self::SessionType::*;
		let result = match *self {
			Plain => false,
			DiffieHellmanSha1 => true,
			DiffieHellmanSha256 => true,
		};
		return result;
	}

	pub fn mac_size(&self) -> usize {
		use self::SessionType::*;
		let result = match *self {
			Plain => ASSOCIATION_PLAIN_MAC_LENGTH,
			DiffieHellmanSha1 => 160,
			DiffieHellmanSha256 => 256,
		};
		return result;
	}
}
