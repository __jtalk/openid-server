// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

pub mod storage;
pub mod data;

use std::ops::Deref;
use std::ops::DerefMut;
use std::error::Error;
use std::u32;
use chrono::Duration;
use iron::Response;
use iron::IronResult;
use iron::IronError;
use iron::status::Status;
use num::bigint::*;
use num::ToPrimitive;
use configuration::Configuration;
use utils::random::*;
use self::storage::AssociationMetadata;
use self::data::*;
use super::fields::*;
use super::utils::*;
use super::utils::http::*;
use super::utils::base64::*;
use super::utils::btwoc::*;
use super::utils::crypto::*;
use super::error::*;
use super::validator::OpenIdVersion;

const ASSOCIATION_HANDLE_LENGTH: usize = 255;
const ASSOCIATION_DURATION_DEFAULT: u64 = 60;

pub fn associate(ver: &OpenIdVersion, body: &Body, ctx: &OpenIdContext) -> IronResult<Response> {
	let a_type = try_or!(association_type(body), OpenIdError::UnsupportedAssociationType);
	let s_type = try_or!(session_type(body), OpenIdError::UnsupportedSessionType);
	if s_type.is_diffie_hellman() {
		return associate_dh(ver, &a_type, &s_type, body, ctx);
	} else {
		return associate_plain(ver, &a_type, &s_type, body, ctx);
	}
}

fn session_type(body: &Body) -> Option<SessionType> {
	let result = extract_typed(body, SESSION_TYPE, |v| SessionType::from_request(v));
	return result;
}

fn association_type(body: &Body) -> Option<AssociationType> {
	let result = extract_typed(body, ASSOCIATION_TYPE, |v| AssociationType::from_request(v));
	return result;
}

fn associate_plain(ver: &OpenIdVersion, a_type: &AssociationType, s_type: &SessionType, _: &Body, ctx: &OpenIdContext) -> IronResult<Response> {
	let assoc_id: String;
	let mac_raw: Vec<u8>;
	{
		let mut guard = ctx.secure_random.lock().unwrap();
		let mut rng = guard.deref_mut();
		assoc_id = random_alnum_case_sensitive(rng, ASSOCIATION_HANDLE_LENGTH);
		mac_raw = random_bytes(rng, s_type.mac_size());
	}
	let expires_in = try!(load_expiration(ctx.configuration.deref()));
	let mac_key = BigUint::from_bytes_be(&mac_raw[..]);
	let state = AssociationMetadata {
		session_type: *s_type,
		association_type: *a_type,
		protocol_version: *ver,
		expires_at: ctx.time.now_utc() + expires_in,
		mac_key: mac_key.clone(),
		session_key: None,
	};
	ctx.associations.replace(assoc_id.as_str(), state);
	let mac_key_base64 = base64(&mac_key.to_bytes_be()[..]);
	let expires_in_str = format!("{}", expires_in.num_seconds());
	let mut response_data = basic_association_response(ver, a_type, s_type, assoc_id.as_str(), expires_in_str.as_str());
	response_data.push((ASSOCIATION_UNENCRYPTED_MAC_RESPONSE, &mac_key_base64));
	let response = create_response(Status::Ok, &response_data[..]);
	return Ok(response);
}

fn associate_dh(ver: &OpenIdVersion, a_type: &AssociationType, s_type: &SessionType, body: &Body, ctx: &OpenIdContext) -> IronResult<Response> {

	let modulus_str = try_or!(extract_single(body, DH_MODULUS), OpenIdError::NoDiffieHellmanModulus);
	let base_str = try_or!(extract_single(body, DH_BASE), OpenIdError::NoDiffieHellmanBase);
	let relying_key_str = try_or!(extract_single(body, DH_RELYING_PARTY_KEY), OpenIdError::NoDiffieHellmanRepyingPartyPublic);
	
	let modulus = try_or!(extract_openid_int(modulus_str), OpenIdError::NoDiffieHellmanModulus);
	let base = try_or!(extract_openid_int(base_str), OpenIdError::NoDiffieHellmanBase);
	let relying_key = try_or!(extract_openid_int(relying_key_str), OpenIdError::NoDiffieHellmanRepyingPartyPublic);

	let hash_length = s_type.mac_size();
	let private_key_cap = modulus.to_u32().unwrap_or(u32::MAX);

	let assoc_id: String;
	let mac_raw: Vec<u8>;
	let private_key: u32;
	{

		let mut guard = ctx.secure_random.lock().unwrap();
		let mut rng = guard.deref_mut();
		assoc_id = random_alnum_case_sensitive(rng, ASSOCIATION_HANDLE_LENGTH);
		mac_raw = random_bytes(rng, hash_length);
		private_key = dh_secure_random(rng, private_key_cap);
	}

	let mac = BigUint::from_bytes_be(&mac_raw[..]);
	let server_key = dh_secure_power(base, private_key, &modulus);
	let session_key = dh_secure_power(relying_key, private_key, &modulus);
	let enc_mac_raw = hash_for(s_type, btwoc(&session_key)) ^ mac.clone();
	let expires_in = try!(load_expiration(ctx.configuration.deref()));

	let state = AssociationMetadata {
		session_type: *s_type,
		association_type: *a_type,
		protocol_version: *ver,
		expires_at: ctx.time.now_utc() + expires_in,
		mac_key: mac,
		session_key: Some(session_key),
	};
	ctx.associations.replace(assoc_id.as_str(), state);

	let enc_mac_base64 = base64(&enc_mac_raw.to_bytes_be()[..]);
	let server_key_base64 = base64(&btwoc(&server_key).as_bytes()[..]);
	let expires_in_str = format!("{}", expires_in.num_seconds());

	let mut response_data = basic_association_response(ver, a_type, s_type, assoc_id.as_str(), expires_in_str.as_str());
	response_data.push((ASSOCIATION_ENCRYPTED_MAC_RESPONSE, &enc_mac_base64));
	response_data.push((ASSOCIATION_DH_SERVER_KEY_RESPONSE, &server_key_base64));
	let response = create_response(Status::Ok, &response_data[..]);
	return Ok(response);
}

fn load_expiration(config: &Configuration) -> IronResult<Duration> {
	use std::time::Duration as SDuration;
	let duration = config.load_duration_secs("openid.association.expires.seconds")
		.unwrap_or_else(|| SDuration::from_secs(ASSOCIATION_DURATION_DEFAULT));
	return Duration::from_std(duration)
		.map_err(|e| OpenIdError::ConfigurationError(Box::new(e), format!("Association duration value overflow: {}", e.description())))
		.map_err(|e| IronError::from(e));
}

fn basic_association_response<'a>(
			ver: &'a OpenIdVersion,
			a_type: &'a AssociationType,
			s_type: &'a SessionType,
			association_id: &'a str,
			expires_in: &'a str
		) -> Vec<(&'a str, &'a str)> {

	let response_data = vec![
		(NS_VERSION_RESPONSE, ver.ns_value()),
		(ASSOCIATION_HANDLE_RESPONSE, association_id),
		(SESSION_TYPE_RESPONSE, s_type.to_response()),
		(ASSOCIATION_TYPE_RESPONSE, a_type.to_response()),
		(ASSOCIATION_EXPIRES_IN_RESPONSE, expires_in),
	];
	return response_data;
}

fn hash_for<S: AsRef<str>>(s_type: &SessionType, data: S) -> BigUint {
	use self::data::SessionType::*;
	let hash = match *s_type {
		Plain => panic!("Hashing called for plain session's association, this is an internal error."),
		DiffieHellmanSha1 => sha1(data),
		DiffieHellmanSha256 => sha256(data),
	};
	return BigUint::from_bytes_be(&hash[..]);
}
