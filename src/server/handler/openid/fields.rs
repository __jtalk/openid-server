// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

pub const AUTH_URL_PREFIX: &'static str = "/id";

pub const NS_VERSION_REQUEST: &'static str = "openid.ns";
pub const NS_VERSION_RESPONSE: &'static str = "ns";
pub const MODE: &'static str = "openid.mode";

pub const NS_VERSION_2_0: &'static str = "http://specs.openid.net/auth/2.0";
pub const NS_VERSION_1_1: &'static str = "http://openid.net/signon/1.1";
pub const NS_VERSION_1_0: &'static str = "http://openid.net/signon/1.0";

pub const MODE_ASSOCIATE: &'static str = "associate";

pub const SESSION_TYPE: &'static str = "openid.session_type";
pub const SESSION_TYPE_RESPONSE: &'static str = "session_type";
pub const SESSION_TYPE_PLAIN: &'static str = "no-encryption";
pub const SESSION_TYPE_DH_SHA1: &'static str = "DH-SHA1";
pub const SESSION_TYPE_DH_SHA256: &'static str = "DH-SHA256";

pub const ASSOCIATION_TYPE: &'static str = "openid.assoc_type";
pub const ASSOCIATION_TYPE_RESPONSE: &'static str = "assoc_type";
pub const ASSOCIATION_TYPE_HMAC_SHA1: &'static str = "HMAC-SHA1";
pub const ASSOCIATION_TYPE_HMAC_SHA256: &'static str = "HMAC-SHA256";

pub const DH_MODULUS: &'static str = "openid.dh_modulus";
pub const DH_BASE: &'static str = "openid.dh_gen";
pub const DH_RELYING_PARTY_KEY: &'static str = "openid.dh_consumer_public";

pub const ASSOCIATION_HANDLE_RESPONSE: &'static str = "assoc_handle";
pub const ASSOCIATION_EXPIRES_IN_RESPONSE: &'static str = "expires_in";

pub const ASSOCIATION_UNENCRYPTED_MAC_RESPONSE: &'static str = "mac_key";
pub const ASSOCIATION_ENCRYPTED_MAC_RESPONSE: &'static str = "enc_mac_key";
pub const ASSOCIATION_DH_SERVER_KEY_RESPONSE: &'static str = "dh_server_public";

pub const ERROR_DESCRIPTION_RESPONSE: &'static str = "error";
pub const ERROR_CODE_RESPONSE: &'static str = "error_code";
pub const ERROR_CODE_UNSUPPORTED_TYPE_RESPONSE: &'static str = "unsupported-type";
pub const ERROR_SESSION_TYPE_RESPONSE: &'static str = "session_type";
pub const ERROR_ASSOCIATION_TYPE_RESPONSE: &'static str = "assoc_type";