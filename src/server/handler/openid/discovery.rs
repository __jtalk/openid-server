// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate router;

use std::ops::Deref;
use std::sync::Arc;
use iron::Handler;
use iron::prelude::*;
use iron::Url;
use self::router::Router;
use iron::mime::*;
use iron::status::Status;
use rustache::HashBuilder;
use configuration::Configuration;
use server::handler::load_base;
use server::templating::*;
use utils::time::TimeSource;
use super::AUTH_URL_PREFIX;

pub struct OpenIdDiscoveryHandler(Arc<Configuration>, Arc<TimeSource>);
impl OpenIdDiscoveryHandler {

	pub fn new(config: Arc<Configuration>, time: Arc<TimeSource>) -> OpenIdDiscoveryHandler {
		return OpenIdDiscoveryHandler(config, time);
	}

	fn config(&self) -> &Configuration {
		return self.0.deref();
	}

	fn time(&self) -> &TimeSource {
		return self.1.deref();
	}
}
impl Handler for OpenIdDiscoveryHandler {
	fn handle(&self, req: &mut Request) -> IronResult<Response> {

		let user = req.extensions.get::<Router>().unwrap().find("user").unwrap_or("");
		let mime = Mime(TopLevel::Text, SubLevel::Html, vec![]);

		let mut data = HashBuilder::new();
		data = load_base(data, self.config(), self.time());
		data = data.insert_string("user", user);
		data = data.insert_string("auth_endpoint", format_auth_url(&req.url, user));

		let result = try!(get_templated_vec("template/discovery.mustache", data));
		let response = Response::with((Status::Ok, result, mime));

		return Ok(response);
	}
}

fn format_auth_url(url: &Url, user: &str) -> String {
	let host = url.host();
	let port = url.port();
	let scheme = url.scheme();
	let result = format!("{}://{}{}{}/{}", scheme, host, port, AUTH_URL_PREFIX, user);
	return result;
}