// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::error::Error;
use std::fmt::Display;
use std::fmt::Formatter;
use std::fmt::Result as FmtResult;
use iron::status::Status;
use iron::mime::Mime;
use iron::IronError;
use server::handler::openid::fields::*;
use server::handler::openid::associate::data::*;
use server::handler::openid::validator::OpenIdVersion;
use server::handler::openid::utils::http::create_response_modifier;

#[derive(Debug)]
pub enum OpenIdError {
	UnsupportedVersion,
	UnsupportedMode,
	UnsupportedSessionType,
	UnsupportedAssociationType,
	NoDiffieHellmanModulus,
	NoDiffieHellmanBase,
	NoDiffieHellmanRepyingPartyPublic,
	ErrorGeneratingSessionSecret,
	ConfigurationError(Box<Error + Send>, String),
}

impl OpenIdError {
	pub fn response_code(&self) -> Status {
		use self::OpenIdError::*;
		let result = match *self {
			UnsupportedVersion => Status::BadRequest,
			UnsupportedMode => Status::BadRequest,
			UnsupportedSessionType => Status::BadRequest,
			UnsupportedAssociationType => Status::BadRequest,
			NoDiffieHellmanModulus => Status::BadRequest,
			NoDiffieHellmanBase => Status::BadRequest,
			NoDiffieHellmanRepyingPartyPublic => Status::BadRequest,
			ErrorGeneratingSessionSecret => Status::InternalServerError,
			ConfigurationError(_, _) => Status::InternalServerError,
		};
		return result;
	}
}

impl Error for OpenIdError {

	fn description(&self) -> &str {
		use self::OpenIdError::*;
		let cause_description = match *self {
			UnsupportedVersion => "This OpenID protocol version is not supported",
			UnsupportedMode => "This OpenID protocol mode is not supported",
			UnsupportedSessionType => "This OpenID session type is not supported",
			UnsupportedAssociationType => "This OpenID association type is not supported",
			NoDiffieHellmanModulus => "This OpenID association request misses Diffie-Hellman modulus",
			NoDiffieHellmanBase => "This OpenID association request misses Diffie-Hellman base",
			NoDiffieHellmanRepyingPartyPublic => "This OpenID association request misses relying party's Diffie-Hellman public key",
			ErrorGeneratingSessionSecret => "The server was unable to generate a unique session ID for this association",
			ConfigurationError(_, ref msg) => msg.as_str(),
		};
		return cause_description;
	}

	fn cause(&self) -> Option<&Error> {
		use self::OpenIdError::*;
		match *self {
			UnsupportedVersion => return None,
			UnsupportedMode => return None,
			UnsupportedSessionType => return None,
			UnsupportedAssociationType => return None,
			NoDiffieHellmanModulus => return None,
			NoDiffieHellmanBase => return None,
			NoDiffieHellmanRepyingPartyPublic => return None,
			ErrorGeneratingSessionSecret => return None,
			ConfigurationError(ref e, _) => return Some(e.as_ref()),
		};
	}
}

///
/// Required by the Error trait.
///
impl Display for OpenIdError {
	fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
		return write!(fmt, "{}", self.description());
	}
}

///
/// We just provide a string now. Should be replaced with JSON error or something.
///
/// TODO: Content negotiation would work great, but it Rust have a nice library for such purposes?
///
impl From<OpenIdError> for IronError {
	fn from(e: OpenIdError) -> IronError {
		use server::errors::iron_error_response;
		let response = format_openid_error(&e);
		return iron_error_response(e, response);
	}
}

fn format_openid_error(e: &OpenIdError) -> (Status, String, Mime) {
	use self::OpenIdError::*;
	let status = e.response_code();
	let mut data = vec![
		(NS_VERSION_RESPONSE, OpenIdVersion::OpenID_2_0.ns_value()),
		(ERROR_DESCRIPTION_RESPONSE, e.description()),
	];
	match *e {
		UnsupportedAssociationType => {
			data.push((ERROR_ASSOCIATION_TYPE_RESPONSE, AssociationType::HmacSha256.to_response()));
			data.push((ERROR_CODE_RESPONSE, ERROR_CODE_UNSUPPORTED_TYPE_RESPONSE));
		},
		UnsupportedSessionType => {
			data.push((ERROR_SESSION_TYPE_RESPONSE, SessionType::DiffieHellmanSha256.to_response()));
			data.push((ERROR_CODE_RESPONSE, ERROR_CODE_UNSUPPORTED_TYPE_RESPONSE));
		},
		_ => (),
	}
	return create_response_modifier(status, &data[..]);
}