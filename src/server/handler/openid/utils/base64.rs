// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::convert::AsRef;
use rustc_serialize::base64::FromBase64;
use rustc_serialize::base64::ToBase64;
use rustc_serialize::base64::STANDARD;

pub fn base64<V: ?Sized>(value: &V) -> String
	where V: ToBase64
{
	let result = value.to_base64(STANDARD);
	return result;
}

///
/// Revert Base64 encoding.
///
/// We skip the error part since it can only be caused
/// by tremendously wrong input. We shall still inform
/// it through logs though.
///
/// Base64 is used to send session keys in OpenID, so we
/// must never log it's contents.
///
pub fn unbase64<S>(value: S) -> Option<Vec<u8>>
	where S: AsRef<str>
{
	let result = value.as_ref().from_base64();
	if let Err(e) = result {
		debug!("Error decoding base64 string: {}. Content is skipped since it may be confidential", e);
		return None;
	} else {
		return result.ok();
	}
}