// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

pub mod crypto;
pub mod btwoc;
pub mod base64;
pub mod http;

use std::sync::Arc;
use std::sync::Mutex;
use std::fmt::Display;
use std::borrow::Borrow;
use std::hash::Hash;
use std::collections::HashMap;
use rand::Rng;
use num::bigint::BigUint;
use configuration::Configuration;
use utils::time::TimeSource;
use self::base64::*;
use self::btwoc::*;
use super::associate::storage::Associations;

pub type Body = HashMap<String, Vec<String>>;

pub fn extract_single<'a, K: ?Sized>(m: &'a Body, name: &K) -> Option<&'a String>
	where String: Borrow<K>, K: Hash + Eq
{
	let result = m.get(name)
		.and_then(|v| v.get(0));
	return result;
}

pub fn extract_openid_int<V: AsRef<str>>(value: V) -> Option<BigUint> {
	let unb64 = opt!(unbase64(value));
	let s = opt!(String::from_utf8(unb64).ok());
	let result = unbtwoc(s);
	return result;
}

pub fn extract_typed<M, K: ?Sized, R>(body: &Body, name: &K, match_fn: M) -> Option<R>
	where M: Fn(&str) -> Option<R>, String: Borrow<K>, K: Display + Hash + Eq
{
	let s_type = opt!(extract_single(body, name));
	let result = match_fn(s_type.as_ref());
	if let None = result {
		warn!("Value from the request matches nothing: {} is '{}'", name, s_type);
	}
	return result;
}

#[derive(Clone)]
pub struct OpenIdContext {
	pub configuration: Arc<Configuration>,
	pub secure_random: Arc<Mutex<Rng + Send>>,
	pub associations: Arc<Associations>,
	pub time: Arc<TimeSource>,
}