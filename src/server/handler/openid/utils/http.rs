// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use iron::Response;
use iron::mime::*;
use iron::status::Status;
use url::form_urlencoded::Serializer;

pub fn create_response(status: Status, data: &[(&str, &str)]) -> Response {
	let modifier = create_response_modifier(status, data);
	return Response::with(modifier);
}

pub fn create_response_modifier(status: Status, data: &[(&str, &str)]) -> (Status, String, Mime) {
	let body = create_urlencoded(data);
	let mime = Mime(TopLevel::Application, SubLevel::WwwFormUrlEncoded, vec![]);
	return (status, body, mime);
}

pub fn create_urlencoded(data: &[(&str, &str)]) -> String {
	let data_size = data.iter()
		.map(|&(ref l, ref r)| l.len() + r.len() + "&".len())
		.map(|len| len * 2) // in case of URLEncoding for some chars, just an estimation
		.fold(0_usize, |a, b| a + b);
	let mut serializer = Serializer::new(String::with_capacity(data_size));
	serializer.extend_pairs(data.iter());
	let result = serializer.finish();
	return result;
}