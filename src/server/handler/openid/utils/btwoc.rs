// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use num::BigUint;

const BTWOC_FORMAT_PREFIX: &'static str = r#"\x"#;

pub fn btwoc(value: &BigUint) -> String {
	let bytes: Vec<u8> = value.to_bytes_be();
	let mut result = Vec::with_capacity(bytes.len());
	for b in bytes.iter() {
		let byte_string = format!(r#"{}{:02X}"#, BTWOC_FORMAT_PREFIX, b);
		result.push(byte_string);
	}
	return result.join("");
}

pub fn unbtwoc<V>(value: V) -> Option<BigUint>
	where V: AsRef<str>
{
	let bytes_vec = value.as_ref()
		.split(BTWOC_FORMAT_PREFIX)
		.collect::<Vec<&str>>();
	let bytes = bytes_vec.join("");
	let result = BigUint::parse_bytes(bytes.as_bytes(), 16);
	return result;
}

#[cfg(test)]
pub mod btwoc_test {

	use super::*;
	use num::bigint::BigUint;

	#[test]
	pub fn test_empty() {
		let value = BigUint::from(0x0_u64);
		let result = btwoc(&value);
		assert_eq!(r#"\x00"#, result);
	}

	#[test]
	pub fn test_two_bytes() {
		let value = BigUint::from(0x5e4f_u64);
		let result = btwoc(&value);
		assert_eq!(r#"\x5E\x4F"#, result);
	}
}

#[cfg(test)]
pub mod unbtwoc_test {

	use super::*;
	use num::bigint::ToBigUint;
	use num::bigint::BigUint;

	#[test]
	pub fn test_empty() {
		let result = unbtwoc(r#"\x00"#);
		assert_eq!(0.to_biguint().unwrap(), result.unwrap());
	}

	#[test]
	pub fn test_two_bytes() {
		let result = unbtwoc(r#"\x5E\x4F"#);
		assert_eq!(0x5e4f_u64.to_biguint().unwrap(), result.unwrap());
	}

	#[test]
	pub fn test_invalid_sequence() {
		let result = unbtwoc(r#"\x5E\x4g"#);
		assert_eq!(None, result);
	}
}