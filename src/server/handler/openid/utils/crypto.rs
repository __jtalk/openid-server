// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate crypto;
extern crate core;

use std::convert::AsRef;
use num::One;
use num::Zero;
use std::ops::Mul;
use std::ops::Rem;
use rand::Rng;
use self::crypto::sha1::Sha1;
use self::crypto::sha2::Sha256;
use self::crypto::digest::Digest;

pub fn sha1<S>(value: S) -> Vec<u8>
	where S: AsRef<str>
{
	return hash(value, Sha1::new());
}

pub fn sha256<S>(value: S) -> Vec<u8>
	where S: AsRef<str>
{
	return hash(value, Sha256::new());
}

fn hash<S, D: Digest>(value: S, mut digest: D) -> Vec<u8>
	where S: AsRef<str>
{
	let data = value.as_ref();
	digest.input_str(data);
	let output_size = digest.output_bytes();
	let mut out = vec![0; output_size];
	digest.result(&mut out);
	return out;
}

///
/// We can only use u32 because num::pow creators use usize
/// as an exponent type. Why would anyone bind exponentation
/// to the underlying platform's pointer size is still a mystery.
///
/// We avoid low exponent values for DH here. Low exponent values may ease
/// key computation, and zero will turn any input into 1.
///
pub fn dh_secure_random(rng: &mut Rng, limit: u32) -> u32 {

	const MIN: u32 = 10000;

	let value = rng.next_u32();
	let result = value % (limit - MIN) + MIN;
	return result;
}

///
/// This function is intended to consume value by performing
/// a volatile write of it's value's derivative. volatile
/// writes must be neither cut-out or reordered by the compiler,
/// so everything should be all right.
///
pub fn consume_noneliminating<T: One + PartialEq>(value: T) {
	use std::ptr::write_volatile;
	let consume_value = value == T::one();
	let mut discard = 10_u32;
	unsafe {
		let discard_ptr = &mut discard as *mut u32;
		write_volatile(discard_ptr, consume_value as u32);
	}
}

///
/// Constant time modulus exponentation function.
///
/// This function takes same amount of time calculating power despite 'power' value.
///
pub fn dh_secure_power<V: Clone + PartialEq<V> + Zero + One + Mul<V, Output = V> + Rem<V, Output = V>>(base: V, power: u32, modulus: &V) -> V {

	use std::u32::MAX as MAX_POWER;

	if base == V::zero() {
		panic!("Zero base supplied to the Diffie-Hellman secure exponentation");
	}
	if base == V::one() {
		panic!("Equals-one base supplied to the Diffie-Hellman secure exponentation");
	}
	if power <= 1 {
		panic!("Zero or one power supplied to the Diffie-Hellman secure exponentation: {}", power);
	}

	let result = dh_insecure_power(base.clone(), power, modulus);
	let avoid_time_attack = dh_insecure_power(base, MAX_POWER - power, modulus);

	consume_noneliminating(avoid_time_attack);

	return result;
}

fn dh_insecure_power<V: Clone + PartialEq<V> + Zero + One + Mul<V, Output = V> + Rem<V, Output = V>>(mut base: V, mut power: u32, modulus: &V) -> V {
	let mut total = V::one();
	while power > 0 {
		if power & 1 == 1 {
			total = (total * base.clone()) % modulus.clone();
		}
		power >>= 1;
		base = (base.clone() * base) % modulus.clone();
	}
	return total;
}

#[cfg(test)]
pub mod test_dh_power {

	use super::*;
	use num::bigint::BigUint;
	use num::pow;
	use num::FromPrimitive;

	#[test]
	pub fn test_pow_max() {
		use std::u32::MAX;
		let value = BigUint::from_u32(10).unwrap();
		let modulus = BigUint::from_u32(3571).unwrap();
		let power = MAX;
		let result = dh_secure_power(value, power, &modulus);
		assert_eq!(BigUint::from_u32(804).unwrap(), result);
	}

	#[test]
	pub fn test_pow_half() {
		use std::u32::MAX;
		let value = BigUint::from_u32(10).unwrap();
		let modulus = BigUint::from_u32(3571).unwrap();
		let power = MAX / 2;
		let result = dh_secure_power(value, power, &modulus);
		assert_eq!(BigUint::from_u32(2999).unwrap(), result);
	}

	#[test]
	pub fn test_pow_min() {
		use std::u32::MAX;
		let value = BigUint::from_u32(10).unwrap();
		let modulus = BigUint::from_u32(3571).unwrap();
		let power = 2;
		let result = dh_secure_power(value, power, &modulus);
		assert_eq!(BigUint::from_u32(100).unwrap(), result);
	}
}