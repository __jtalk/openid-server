// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::sync::Arc;
use std::ops::Deref;
use iron::*;
use iron::status::Status;
use configuration::Configuration;

pub struct UserHandler(Arc<Configuration>);
impl UserHandler {

	pub fn new(config: Arc<Configuration>) -> UserHandler {
		return UserHandler(config);
	}

	#[allow(dead_code)]
	fn config(&self) -> &Configuration {
		return self.0.deref();
	}
}
impl Handler for UserHandler {
	fn handle(&self, _: &mut Request) -> IronResult<Response> {
		return Ok(Response::with((Status::Ok, "Welcome to the Rust OpenID server's user information page!")));
	}
}