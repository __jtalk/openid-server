// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::ops::Deref;
use std::sync::Arc;
use iron::*;
use iron::mime::*;
use iron::status::Status;
use rustache::HashBuilder;
use configuration::Configuration;
use server::handler::load_base;
use server::templating::*;
use utils::time::TimeSource;

///
/// A main page handler
///
pub struct WelcomeHandler(Arc<Configuration>, Arc<TimeSource>);
impl WelcomeHandler {

	pub fn new(config: Arc<Configuration>, time: Arc<TimeSource>) -> WelcomeHandler {
		return WelcomeHandler(config, time);
	}

	fn config(&self) -> &Configuration {
		return self.0.deref();
	}

	fn time(&self) -> &TimeSource {
		return self.1.deref();
	}
}
impl Handler for WelcomeHandler {
	fn handle(&self, _: &mut Request) -> IronResult<Response> {

		let title = self.config().load_string_or(&"page.welcome.title", &"Unnamed OpenID Server");
		let mime = Mime(TopLevel::Text, SubLevel::Html, vec![]);

		let mut data = HashBuilder::new();
		data = load_base(data, self.config(), self.time());
		data = data.insert_string("title", title);

		let result = try!(get_templated_vec("template/welcome.mustache", data));
		let response = Response::with((Status::Ok, result, mime));

		return Ok(response);
	}
}