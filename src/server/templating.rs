
// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::io::Read;
use std::convert::From;
use std::fmt::Debug;
use std::result::Result as StdResult;
use rustache::{render_file, Render};
use server::errors::render::*;

/// Render a template as string.
///
/// This is a convenient wrapper around render_file to avoid string extraction
/// code duplication.
///
/// If you don't need string representation, get_templated_vec is a preferred
/// solution as it avoids extra conversion string version performs.
///
/// # Example
///
/// To be used with Iron Framework as a rendering backend, in handle() function:
///
/// let rendered = try!(get_templated_str("templates/main.mustache", HashBuilder::new()));
/// return Response::with((Status::Ok, rendered));
///
#[allow(dead_code)]
pub fn get_templated_str<R: Read, Re: Render<R> + Debug>(path: &str, renderable: Re) -> RenderResult<String> {
	let vec = try!(get_templated_vec(path, renderable));
	let text = try!(String::from_utf8(vec));
	return StdResult::Ok(text);
}

/// Render a template as raw bytes vector.
///
/// This is a convenient wrapper around render_file to avoid vector extraction
/// code duplication.
///
/// # Example
///
/// To be used with Iron Framework as a rendering backend, in handle() function:
///
/// let rendered = try!(get_templated_vec("templates/main.mustache", HashBuilder::new()));
/// return Response::with((Status::Ok, rendered));
///
pub fn get_templated_vec<R: Read, Re: Render<R>>(path: &str, renderable: Re) -> RenderResult<Vec<u8>> {
	let mut body_raw = try!(render_file(path, renderable));
	let mut result = Vec::new();
	let read = body_raw.read_to_end(&mut result);
	if let StdResult::Err(e) = read {
		return StdResult::Err(RenderError::from(e));
	} else {
		return StdResult::Ok(result);
	}
}