
// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

pub mod render;
pub mod body_parse;

use iron::*;
use iron::modifier::Modifier;
use iron::status::Status;
use iron::mime::*;
	
///
/// A convenient function for IronError construction from a general error type.
///
/// TODO: Review it's MIME type. We might need some content negotiation here which
/// may break the outer interface.
///
pub fn iron_error<E: Error>(error: E, status: Status, description: String) -> IronError {
	let response = (status, description, Mime(TopLevel::Text, SubLevel::Plain, vec![]));
	let error = IronError::new(error, response);
	return error;
}

pub fn iron_error_response<E: Error, R: Modifier<Response>>(error: E, response: R) -> IronError {
	let error = IronError::new(error, response);
	return error;
}
