
// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

pub mod rustache_error;

use std::io::Error as StdIoError;
use std::error::Error;
use std::convert::From;
use std::fmt::Display;
use std::fmt::Formatter;
use std::fmt::Result as FmtResult;
use std::result::Result as StdResult;
use std::string::FromUtf8Error;
use iron::IronError;
use rustache::RustacheError as RustacheLLError;
use self::rustache_error::RustacheErrorWrapper;

///
/// A generalized wrapper around different rendering errors.
///
/// In our renderer we may get different kinds of errors. In accordance
/// with Rust guidelines, we ought to implement a convenient wrapper around
/// them to be returned in our custom RenderResult.
///
/// Those wrappers keep textual representation of an error in order to
/// provide a reference to it in Error.description calls. RustacheErrorWrapper
/// already has such a string inside it and thus requires no extra strings. I should
/// rethink the latter since such an exception breaks generalization and makes a
/// coupling between renderer and rustache_error a little bit tight.
///
#[derive(Debug)]
pub enum RenderError {
	RustacheError(RustacheErrorWrapper),
	StringParsingError(FromUtf8Error, String),
	IoError(StdIoError, String),
}

///
/// A result type for server::templating rendereres.
///
/// In accordance with Rust practices I ought to place those
/// declarations into the server::templating module. However,
/// my Java backgrounds tells different.
///
pub type RenderResult<T> = StdResult<T, RenderError>;

impl Error for RenderError {

	fn description(&self) -> &str {
		use self::RenderError::*;
		let cause_description = match *self {
			RustacheError(ref wrapper) => wrapper.description(),
			StringParsingError(_, ref desc) => desc.as_ref(),
			IoError(_, ref desc) => desc.as_ref(),
		};
		return cause_description;
	}

	fn cause(&self) -> Option<&Error> {
		use self::RenderError::*;
		match *self {
			RustacheError(ref e) => return Some(e),
			StringParsingError(ref e, _) => return Some(e),
			IoError(ref e, _) => return Some(e),
		};
	}
}

///
/// Required by the Error trait.
///
impl Display for RenderError {
	fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
		return write!(fmt, "{}", self.description());
	}
}

impl From<RustacheLLError> for RenderError {
	fn from(e: RustacheLLError) -> RenderError {
		return RenderError::RustacheError(RustacheErrorWrapper::from(e));
	}
}
impl From<FromUtf8Error> for RenderError {
	fn from(e: FromUtf8Error) -> RenderError {
		let desc = format!("[FromUtf8Error]: {:?}", e);
		return RenderError::StringParsingError(e, desc);
	}
}
impl From<StdIoError> for RenderError {
	fn from(e: StdIoError) -> RenderError {
		let desc = format!("[io::Error]: {:?}", e);
		return RenderError::IoError(e, desc);
	}
}

///
/// We just provide a string now. Should be replaced with JSON error or something.
///
/// TODO: Content negotiation would work great, but it Rust have a nice library for such purposes?
///
impl From<RenderError> for IronError {
	fn from(e: RenderError) -> IronError {
		use iron::status::Status;
		use server::errors::iron_error;
		return iron_error(e, Status::InternalServerError, "An error occured during response rendering".to_string());
	}
}