
// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::error::Error;
use std::convert::From;
use std::fmt::Display;
use std::fmt::Formatter;
use std::fmt::Result as FmtResult;
use rustache::RustacheError as RustacheLLError;

///
/// A convenient wrapper around RustacheError to implement the Error trait.
///
/// Since Rustache developers make no effort implementing convenient traits
/// for their errors, we need to do it ourselves.
///
#[derive(Debug)]
pub struct RustacheErrorWrapper(RustacheLLError, String);

impl Error for RustacheErrorWrapper {
	fn description<'a>(&'a self) -> &'a str {
		let &RustacheErrorWrapper(_, ref dsc) = self;
		return dsc.as_ref();
	}
}

///
/// Error trait requires this.
///
impl Display for RustacheErrorWrapper {
	fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
		return write!(fmt, "{}", self.description());
	}
}

impl From<RustacheLLError> for RustacheErrorWrapper {
	fn from(src: RustacheLLError) -> RustacheErrorWrapper {
		let desc = format!("[RustacheError]: {:?}", src).to_string();
		return RustacheErrorWrapper(src, desc);
	}
}