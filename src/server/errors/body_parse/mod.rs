
// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate urlencoded;

use std::error::Error;
use std::convert::From;
use std::fmt::Display;
use std::fmt::Formatter;
use std::fmt::Result as FmtResult;
use iron::IronError;
use self::urlencoded::UrlDecodingError;

#[derive(Debug)]
pub enum BodyParseError {
	Err(UrlDecodingError, String),
}

impl Error for BodyParseError {

	fn description(&self) -> &str {
		use self::BodyParseError::*;
		let cause_description = match *self {
			Err(_, ref desc) => desc.as_ref(),
		};
		return cause_description;
	}

	fn cause(&self) -> Option<&Error> {
		use self::BodyParseError::*;
		match *self {
			Err(ref e, _) => return Some(e),
		};
	}
}

///
/// Required by the Error trait.
///
impl Display for BodyParseError {
	fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
		return write!(fmt, "{}", self.description());
	}
}

impl From<UrlDecodingError> for BodyParseError {
	fn from(e: UrlDecodingError) -> BodyParseError {
		let desc = format!("[UrlDecodingError]: {:?}", e);
		return BodyParseError::Err(e, desc);
	}
}

///
/// We just provide a string now. Should be replaced with JSON error or something.
///
/// TODO: Content negotiation would work great, but it Rust have a nice library for such purposes?
///
impl From<BodyParseError> for IronError {
	fn from(e: BodyParseError) -> IronError {
		use iron::status::Status;
		use server::errors::iron_error;
		return iron_error(e, Status::InternalServerError, "An error occured during request parsing".to_string());
	}
}