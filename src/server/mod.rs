
// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate router;

pub mod handler;
pub mod templating;
pub mod errors;

mod static_handler;

use std::ops::Deref;
use std::sync::Arc;
use std::sync::Mutex;
use std::result::Result as StdResult;
use rand::os::OsRng;
use iron::*;
use self::router::*;
use self::handler::welcome::WelcomeHandler;
use self::handler::user::UserHandler;
use self::handler::openid::OpenIdHandler;
use self::handler::openid::discovery::OpenIdDiscoveryHandler;
use self::handler::openid::associate::storage::InMemoryAssociations;
use self::handler::openid::utils::OpenIdContext;
use self::static_handler::StaticHandler;
use configuration::Configuration;
use utils::time::TimeSource;

pub struct Server {
	#[allow(dead_code)]
	listening: Listening,
}

///
/// Since server instantiation is quite simple, we only need a string
/// to describe any error occured.
///
pub type ServerResult = StdResult<Server, String>;

const LISTEN_HOST_PARAM: &'static str = "listen.host";
const LISTEN_PORT_PARAM: &'static str = "listen.port";
const HANDLER_THREADS_PARAM: &'static str = "listen.handler.threads";

impl Server {
	///
	/// Despite this method returns Listening instance, which provides
	/// close() method itself, this method is known not to work in Iron's
	/// current implementation, so we do nothing with this value.
	///
	pub fn create(config: &Arc<Configuration>, time: &Arc<TimeSource>) -> ServerResult {
		let handler = create_handler(config, time);
		let iron = Iron::new(handler);
		let addr = try_opt!(config.load_string(LISTEN_HOST_PARAM),
				"Listening host is not configured, consider inspecting property '{}'",
				LISTEN_HOST_PARAM);
		let port = try_opt!(config.load_int64(LISTEN_PORT_PARAM),
				"Listening port is not configured, consider inspecting property '{}'",
				LISTEN_PORT_PARAM) as u16;
		let timeouts = load_timeouts(config.deref());
		let threads = try_opt!(config.load_int64(HANDLER_THREADS_PARAM),
				"Handlers thread count is not configured, consider inspecting property '{}'",
				HANDLER_THREADS_PARAM) as usize;
		info!("Starting server at '{:?}:{}', {} threads, timeouts = {:?}", addr, port, threads, timeouts);
		let listening = iron.listen_with((addr.as_str(), port), threads, Protocol::Http, Some(timeouts)).unwrap();
		let result = Server {
			listening: listening,
		};
		return StdResult::Ok(result);
	}
}

fn create_handler(config: &Arc<Configuration>, time: &Arc<TimeSource>) -> Router {

	let openid_prefix = format!("{}/:user", self::handler::openid::AUTH_URL_PREFIX);
	let openid_ctx = OpenIdContext {
		configuration: config.clone(),
		time: time.clone(),
		associations: Arc::new(InMemoryAssociations::new()),
		secure_random: Arc::new(Mutex::new(OsRng::new().unwrap())),
	};

	let mut handler = Router::new();
	handler.get("/", WelcomeHandler::new(config.clone(), time.clone()));
	handler.get("/profile", UserHandler::new(config.clone()));
	handler.get("/id/:user", OpenIdDiscoveryHandler::new(config.clone(), time.clone()));
	handler.post(openid_prefix, OpenIdHandler::new(openid_ctx));
	handler.get("/static/*", StaticHandler::new(config.clone()));
	return handler;
}

///
/// A complex Timeouts object loading routine
///
/// Since timeouts are important, we load everything we can,
/// and then we MERGE our loaded version with Iron's default one.
///
/// TODO: Review non-static loaders storing. I don't know any better solution for now.
///
fn load_timeouts(config: &Configuration) -> Timeouts {

	use std::time::Duration;

	type Applier = Fn(&mut Timeouts, Duration) -> ();

	let keepalive_applier = |timeouts: &mut Timeouts, value: Duration| timeouts.keep_alive = Some(value);
	let read_applier = |timeouts: &mut Timeouts, value: Duration| timeouts.read = Some(value);
	let write_applier = |timeouts: &mut Timeouts, value: Duration| timeouts.write = Some(value);

	let handlers: [(&str, &Applier); 3] = [
		("listen.timeouts.keep_alive", &keepalive_applier),
		("listen.timeouts.read", &read_applier),
		("listen.timeouts.write", &write_applier),
	];

	let mut timeouts = Timeouts::default();
	for &(name, applier) in handlers.iter() {
		let loaded = config.load_int64(name);
		if let Some(value) = loaded {
			let duration = Duration::from_millis(value as u64);
			applier(&mut timeouts, duration);
		}
	}
	return timeouts;
}