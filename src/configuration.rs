
// Copyright (C) 2016 Roman Nazarenko

// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
// License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate rustc_serialize;

use std::convert::AsRef;
use std::time::Duration;
use std::fs::File;
use std::collections::BTreeMap;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::sync::RwLock;
use self::rustc_serialize::json::Json;
use self::rustc_serialize::json::BuilderError;

type ValueLoader<R> = Fn(&Json) -> Result<R, String>;

pub trait Configuration: Sync + Send {
	fn load_string(&self, key: &str) -> Option<String>;
	fn load_string_or(&self, key: &str, default: &str) -> String;
	fn load_int64(&self, key: &str) -> Option<i64>;
	fn load_f64(&self, key: &str) -> Option<f64>;
	fn load_duration_secs(&self, key: &str) -> Option<Duration>;
	fn load_duration_millis(&self, key: &str) -> Option<Duration>;
	fn load_array_str(&self, key: &str) -> Option<Vec<String>>;
	fn load_array_int64(&self, key: &str) -> Option<Vec<i64>>;
	fn load_array_f64(&self, key: &str) -> Option<Vec<f64>>;
	fn load_map_str(&self, key: &str) -> Option<HashMap<String, String>>;
	fn load_map_int64(&self, key: &str) -> Option<HashMap<String, i64>>;
	fn load_map_f64(&self, key: &str) -> Option<HashMap<String, f64>>;
}

pub struct JsonConfiguration(PathBuf, RwLock<Json>);

impl JsonConfiguration {

	pub fn from_file(path: &Path) -> Result<JsonConfiguration, BuilderError> {
		let json = read_file(path);
		return json.map(|i| JsonConfiguration(path.to_owned(), RwLock::new(i)));
	}

	pub fn reload(&mut self) -> Result<&JsonConfiguration, BuilderError> {
		{
			let &mut JsonConfiguration(ref path, ref mut storage) = self;
			let loaded = try!(read_file(path.as_ref()));
			debug!("Json is: {}", loaded);
			let mut old = storage.write().unwrap();
			*old = loaded;
		}
		debug!("Configuration has been reloaded");
		return Ok(self);
	}

	fn get_json(&self) -> &RwLock<Json> {
		let &JsonConfiguration(_, ref json) = self;
		return json;
	}

	fn with_found<F, R>(&self, key: &str, f: F) -> Option<R>
		where F: Fn(&Json) -> Option<R>
	{
		let json = self.get_json().read().unwrap();
		let found = self.find_by_key(&*json, key);
		let result = found.and_then(|item| f(item));
		return result;
	}

	fn find_by_key<'a>(&self, json: &'a Json, key: &str) -> Option<&'a Json>	{
		let splitted = Self::split_key(key);
		return json.find_path(&splitted[..]);
	}

	fn split_key(key: &str) -> Vec<&str>	{
		return key.split('.').collect();
	}

	fn load_array_with<R>(&self, key: &str, converter: &ValueLoader<R>) -> Option<Vec<R>>
	{
		let result = self.with_found(key, |json| {
			match *json {
				Json::Array(ref a) => {
					let parsed: Result<Vec<R>, String> = Self::parse_array_with(a, converter);
					parsed.map_err(|e| warn!("Unable to parse configuration for key '{:?}': {}", key, e))
						.ok()
				},
				_ => None,
			}
		});
		return result;
	}

	fn load_map_with<R>(&self, key: &str, value_converter: &ValueLoader<R>) -> Option<HashMap<String, R>>
	{
		let result = self.with_found(key, |json| {
			match *json {
				Json::Object(ref a) => {
					let parsed = Self::parse_object_with(a, value_converter);
					parsed.map_err(|e| warn!("Unable to parse configuration for key '{:?}': {}", key, e))
						.ok()
				},
				_ => None,
			}
		});
		return result;
	}

	fn parse_string(from: &Json) -> Result<String, String> {
		let result = match *from {
			Json::String(ref s) => Ok(s.clone()),
			Json::Object(_) => Self::unexpected_type("object", "string"),
			Json::Array(_) => Self::unexpected_type("array", "string"),
			Json::Null => Self::unexpected_type("null", "string"),
			ref v => Ok(format!("{}", v)),
		};
		return result;
	}

	fn parse_int64(from: &Json) -> Result<i64, String> {
		let result = match *from {
			Json::I64(i) => Ok(i),
			Json::U64(i) => Self::to_signed_checked(i),
			Json::F64(i) => Self::to_integral_checked(i),
			ref j => Self::unexpected_type(Self::detect_type(&j), "i64"),
		};
		return result;
	}

	fn parse_f64(from: &Json) -> Result<f64, String> {
		let result = match *from {
			Json::I64(i) => Ok(i as f64),
			Json::U64(u) => Self::to_signed_checked(u).map(|f| f as f64),
			Json::F64(f) => Ok(f),
			ref j => Self::unexpected_type(Self::detect_type(&j), "f64"),
		};
		return result;
	}

	fn parse_array_with<R>(arr: &Vec<Json>, converter: &ValueLoader<R>) -> Result<Vec<R>, String> {
		let mut result = Vec::with_capacity(arr.len());
		for j in arr.iter() {
			let parsed: Result<R, String> = converter(j);
			match parsed {
				Ok(value) => result.push(value),
				Err(e) => return Err(e),
			}
		}
		return Ok(result);
	}

	fn parse_object_with<R>(obj: &BTreeMap<String, Json>, value_converter: &ValueLoader<R>) -> Result<HashMap<String, R>, String> {
		let mut result = HashMap::with_capacity(obj.len());
		for (name, value) in obj.iter() {
			let data: Result<R, String> = value_converter(value);
			match data {
				Ok(d) => { result.insert(name.clone(), d); },
				Err(e) => return Err(e),
			}
		}
		return Ok(result);
	}

	fn to_signed_checked(value: u64) -> Result<i64, String>	{
		if value > i64::max_value() as u64 {
			let message = format!("Value too large to load: '{}', only signed 64-bit integers are supported", value);
			return Err(message);
		} else {
			return Ok(value as i64);
		}
	}

	fn to_integral_checked(value: f64) -> Result<i64, String> {
		if value >= i64::max_value() as f64 {
			let message = format!("Value is too large for float-to-integral conversion: '{}'", value);
			return Err(message);
		}
		let integral = value as i64;
		let is_integral = integral as f64 == value;
		if is_integral {
			return Ok(value as i64);
		} else {
			let message = format!("Value is not integral: '{}'", value);
			return Err(message);
		}
	}

	fn unexpected_type<R>(found_type: &str, expected_type: &str) -> Result<R, String> {
		let message = format!("Unexpected type obtained from config: expected '{}', found '{}'", expected_type, found_type);
		return Err(message);
	}

	fn detect_type(json: &Json) -> &str {
		return match *json {
			Json::I64(_) => "i64",
			Json::U64(_) => "i64",
			Json::F64(_) => "f64",
			Json::String(_) => "string",
			Json::Boolean(_) => "boolean",
			Json::Array(_) => "array",
			Json::Object(_) => "object",
			Json::Null => "null",
		}
	}
}

fn read_file(path: &Path) -> Result<Json, BuilderError> {
	let mut reader = try!(File::open(&path));
	let json = Json::from_reader(&mut reader);
	return json;
}

impl Configuration for JsonConfiguration {

	fn load_string(&self, key: &str) -> Option<String>
	{
		let result = self.with_found(key, |json| {
			let parsed = Self::parse_string(json);
			parsed.map_err(|e| warn!("Problem parsing string key '{:?}': {}", key, e))
				.ok()
		});
		return result;
	}

	fn load_string_or(&self, key: &str, default: &str) -> String {
		return self.load_string(key).unwrap_or_else(|| String::from(default.as_ref()));
	}

	fn load_int64(&self, key: &str) -> Option<i64>
	{
		let result = self.with_found(key, |json| {
			let parsed = Self::parse_int64(json);
			parsed.map_err(|e| warn!("Problem parsing int64 key '{:?}': {}", key, e))
				.ok()
		});
		return result;
	}

	fn load_f64(&self, key: &str) -> Option<f64>
	{
		let result = self.with_found(key, |json| {
			let parsed = Self::parse_f64(json);
			parsed.map_err(|e| warn!("Problem parsing f64 key '{:?}': {}", key, e))
				.ok()
		});
		return result;
	}

	fn load_duration_secs(&self, key: &str) -> Option<Duration>
	{
		return self.load_duration_millis(key).map(|d| d * 1000);
	}

	fn load_duration_millis(&self, key: &str) -> Option<Duration>
	{
		let loaded = self.load_int64(key);
		if let Some(millis) = loaded {
			if millis > 0_i64 {
				return Some(Duration::from_millis(millis as u64));
			} else {
				warn!("Negative integer '{}' loaded as Duration for configuration key '{:?}'", millis, key);
			}
		}
		return None;
	}

	fn load_array_str(&self, key: &str) -> Option<Vec<String>>
	{
		return self.load_array_with(key, &Self::parse_string);
	}

	fn load_array_int64(&self, key: &str) -> Option<Vec<i64>>
	{
		return self.load_array_with(key, &Self::parse_int64);
	}

	fn load_array_f64(&self, key: &str) -> Option<Vec<f64>>
	{
		return self.load_array_with(key, &Self::parse_f64);
	}

	fn load_map_str(&self, key: &str) -> Option<HashMap<String, String>>
	{
		return self.load_map_with(key, &Self::parse_string);
	}

	fn load_map_int64(&self, key: &str) -> Option<HashMap<String, i64>>
	{
		return self.load_map_with(key, &Self::parse_int64);
	}

	fn load_map_f64(&self, key: &str) -> Option<HashMap<String, f64>>
	{
		return self.load_map_with(key, &Self::parse_f64);
	}
}

#[cfg(test)]
#[macro_use]
mod test_macros {
	macro_rules! value_test {
		( $name:ident, $json:expr, $key:expr, $function:ident, $result:expr, $cmp:ident ) => {
			#[test]
			fn $name() {
				let c = JsonConfiguration(Path::new("").to_owned(), RwLock::new(Json::from_str($json).unwrap()));
				let found = c.$function($key);
				$cmp!($result, found.unwrap());
			}
		};
		( $name:ident, $json:expr, $key:expr, $function:ident, $result:expr ) => {
			value_test!($name, $json, $key, $function, $result, assert_eq);
		};
	}

	macro_rules! value_test_default {
		( $name:ident, $json:expr, $key:expr, $function:ident, $result:expr, $default:expr, $cmp:ident ) => {
			#[test]
			fn $name() {
				let c = JsonConfiguration(Path::new("").to_owned(), RwLock::new(Json::from_str($json).unwrap()));
				let found = c.$function($key, $default);
				$cmp!($result, found);
			}
		};
		( $name:ident, $json:expr, $key:expr, $function:ident, $result:expr, $default:expr ) => {
			value_test_default!($name, $json, $key, $function, $result, $default, assert_eq);
		};
	}

	macro_rules! not_found_test {
		( $name:ident, $json:expr, $key:expr, $function:ident ) => {
			#[test]
			fn $name() {
				let c = JsonConfiguration(Path::new("").to_owned(), RwLock::new(Json::from_str($json).unwrap()));
				let found = c.$function($key);
				assert_eq!(None, found);
			}
		};
	}

	macro_rules! duration_test {
		( $name:ident, $json:expr, $key:expr, $function:ident, $result:expr ) => {
			#[test]
			fn $name() {
				let c = JsonConfiguration(Path::new("").to_owned(), RwLock::new(Json::from_str($json).unwrap()));
				let found = c.$function($key);
				assert_eq!($result, found.unwrap().as_secs());
			}
		}
	}
}

#[cfg(test)]
pub mod test_string {

	use std::path::Path;
	use std::sync::RwLock;
	use super::Configuration;
	use super::JsonConfiguration;
	use super::rustc_serialize::json::Json;

	value_test!(load_string_by_ref, r#"{ "test": "test.value" }"#, "test", load_string, "test.value");
	value_test!(load_string_by_string, r#"{ "test": "test.value" }"#, &"test".to_string(), load_string, "test.value");
	value_test!(load_string_by_path, r#"{ "test": { "value": "test.value" } }"#, "test.value", load_string, "test.value");
	value_test!(load_string_from_int, r#"{ "test": 10 }"#, "test", load_string, "10");
	value_test!(load_string_from_float, r#"{ "test": 10.5 }"#, "test", load_string, "10.5");
	value_test!(load_string_from_boolean, r#"{ "test": true }"#, "test", load_string, "true");

	value_test_default!(load_string_by_ref_default_by_ref, r#"{ "nonsense": "test.value" }"#, "test",
		load_string_or, "default.value", "default.value");
	value_test_default!(load_string_by_string_default_by_ref, r#"{ "nonsense": "test.value" }"#, &"test".to_string(),
		load_string_or, "default.value", "default.value");
	value_test_default!(load_string_by_ref_default_by_string, r#"{ "nonsense": "test.value" }"#, "test",
		load_string_or, "default.value", &"default.value".to_string());
	value_test_default!(load_string_by_string_default_by_string, r#"{ "nonsense": "test.value" }"#, &"test".to_string(),
		load_string_or, "default.value", &"default.value".to_string());
	value_test_default!(load_string_no_default, r#"{ "test": "test.value" }"#, "test",
		load_string_or, "test.value", "default.value");

	not_found_test!(load_nothing_from_empty, r#"{ }"#, "test", load_string);
	not_found_test!(load_nothing_from_other, r#"{ "nonsense": "value" }"#, "test", load_string);
	not_found_test!(load_nothing_from_object, r#"{ "test": { "test": "value" } }"#, "test", load_string);
	not_found_test!(load_nothing_from_array, r#"{ "test": [ "test", "value" ] }"#, "test", load_string);
	not_found_test!(load_nothing_from_null, r#"{ "test": null }"#, "test", load_string);
}

#[cfg(test)]
pub mod test_int {

	use std::path::Path;
	use std::sync::RwLock;
	use super::Configuration;
	use super::JsonConfiguration;
	use super::rustc_serialize::json::Json;

	value_test!(load_int_by_ref, r#"{ "test": 10 }"#, "test", load_int64, 10);
	value_test!(load_int_by_string, r#"{ "test": 10 }"#, &"test".to_string(), load_int64, 10);
	value_test!(load_int_by_path, r#"{ "test": { "value": 10 } }"#, "test.value", load_int64, 10);
	value_test!(load_int_negative_passes, r#"{ "test": -10 }"#, "test", load_int64, -10);

	not_found_test!(no_load_by_string, r#"{ "test": "nonsense" }"#, "test", load_int64);
	not_found_test!(no_load_by_object, r#"{ "test": { "test": 10 } }"#, "test", load_int64);
	not_found_test!(no_load_by_array, r#"{ "test": [10] }"#, "test", load_int64);
	not_found_test!(no_load_by_null, r#"{ "test": null }"#, "test", load_int64);
}

#[cfg(test)]
pub mod test_float {

	use std::path::Path;
	use std::sync::RwLock;
	use super::Configuration;
	use super::JsonConfiguration;
	use super::rustc_serialize::json::Json;

	value_test!(load_float_by_ref, r#"{ "test": 10.1 }"#, "test", load_f64, 10.1, assert_float_eq);
	value_test!(load_float_by_string, r#"{ "test": 10.1 }"#, &"test".to_string(), load_f64, 10.1, assert_float_eq);
	value_test!(load_float_by_path, r#"{ "test": { "value": 10.1 } }"#, "test.value", load_f64, 10.1, assert_float_eq);
	value_test!(load_float_negative_passes, r#"{ "test": -10.1 }"#, "test", load_f64, -10.1, assert_float_eq);
	value_test!(load_float_e_notation_passes, r#"{ "test": 192e-3 }"#, "test", load_f64, 0.192, assert_float_eq);

	not_found_test!(no_load_by_string, r#"{ "test": "nonsense" }"#, "test", load_f64);
	not_found_test!(no_load_by_array, r#"{ "test": [192e-3] }"#, "test", load_f64);
	not_found_test!(no_load_by_object, r#"{ "test": { "test": 192e-3} }"#, "test", load_f64);
	not_found_test!(no_load_by_null, r#"{ "test": null }"#, "test", load_f64);

	///
	/// Technically, floating point values should be parsed despite their length,
	/// loosing their precision accordingly. However, rustc_serialize does not provide
	/// such possibilities, making overflow test failing.
	///
	#[test]
	#[ignore]
	fn load_float_no_positive_overflow() {
		let value = u64::max_value() as f64 * 10000_f64;
		let json = format!("{{ \"test\": {} }}", value);
		let parsed = Json::from_str(json.as_str());
		let c = JsonConfiguration(Path::new("").to_owned(), RwLock::new(parsed.unwrap()));
		let found = c.load_f64("test");
		assert_float_eq!(value, found.unwrap());
	}
}

#[cfg(test)]
pub mod test_duration {

	use std::path::Path;
	use std::sync::RwLock;
	use super::Configuration;
	use super::JsonConfiguration;
	use super::rustc_serialize::json::Json;

	duration_test!(load_duration_secs_by_ref, r#"{ "test": 10 }"#, "test", load_duration_secs, 10);
	duration_test!(load_duration_secs_by_string, r#"{ "test": 10 }"#, &"test".to_string(), load_duration_secs, 10);
	duration_test!(load_duration_secs_by_path, r#"{ "test": { "value": 10 } }"#, "test.value", load_duration_secs, 10);
	duration_test!(load_duration_millis_by_ref, r#"{ "test": 1000 }"#, "test", load_duration_millis, 1);
	duration_test!(load_duration_millis_by_string, r#"{ "test": 1000 }"#, &"test".to_string(), load_duration_millis, 1);
	duration_test!(load_duration_millis_by_path, r#"{ "test": { "value": 1000 } }"#, "test.value", load_duration_millis, 1);

	not_found_test!(no_load_by_string_secs, r#"{ "test": "10" }"#, "test", load_duration_secs);
	not_found_test!(no_load_by_array_secs, r#"{ "test": [10] }"#, "test", load_duration_secs);
	not_found_test!(no_load_by_object_secs, r#"{ "test": { "test": 10} }"#, "test", load_duration_secs);
	not_found_test!(no_load_by_null_secs, r#"{ "test": null }"#, "test", load_duration_secs);
	not_found_test!(no_load_by_string_millis, r#"{ "test": "10" }"#, "test", load_duration_millis);
	not_found_test!(no_load_by_array_millis, r#"{ "test": [10] }"#, "test", load_duration_millis);
	not_found_test!(no_load_by_object_millis, r#"{ "test": { "test": 10} }"#, "test", load_duration_millis);
	not_found_test!(no_load_by_null_millis, r#"{ "test": null }"#, "test", load_duration_millis);
}

#[cfg(test)]
pub mod test_array {

	use std::path::Path;
	use std::sync::RwLock;
	use super::Configuration;
	use super::JsonConfiguration;
	use super::rustc_serialize::json::Json;

	value_test!(load_array_str_by_ref, r#"{ "test": [ "a", "b" ] }"#, "test", load_array_str, vec!["a", "b"]);
	value_test!(load_array_str_by_string, r#"{ "test": [ "a", "b" ] }"#, &"test".to_string(), load_array_str, vec!["a", "b"]);
	value_test!(load_array_str_by_path, r#"{ "test": { "value": [ "a", "b" ] } }"#, "test.value", load_array_str, vec!["a", "b"]);

	value_test!(load_array_int64_by_ref, r#"{ "test": [ 2, -1, 3.0 ] }"#, "test", load_array_int64, vec![2, -1, 3]);
	value_test!(load_array_int64_by_string, r#"{ "test": [ 2, -1, 3.0 ] }"#, &"test".to_string(), load_array_int64, vec![2, -1, 3]);
	value_test!(load_array_int64_by_path, r#"{ "test": { "value": [ 2, -1, 3.0 ] } }"#, "test.value", load_array_int64, vec![2, -1, 3]);

	value_test!(load_array_f64_by_ref, r#"{ "test": [ 2, 3.14, -5 ] }"#, "test", load_array_f64, vec![2.0, 3.14, -5.0]);
	value_test!(load_array_f64_by_string, r#"{ "test": [ 2, 3.14, -5 ] }"#, &"test".to_string(), load_array_f64, vec![2.0, 3.14, -5.0]);
	value_test!(load_array_f64_by_path, r#"{ "test": { "value": [ 2, 3.14, -5 ] } }"#, "test.value", load_array_f64, vec![2.0, 3.14, -5.0]);

	not_found_test!(no_load_array_str_by_string, r#"{ "test": "[a, b]" }"#, "test", load_array_str);
	not_found_test!(no_load_array_str_by_int, r#"{ "test": 192e-3 }"#, "test", load_array_str);
	not_found_test!(no_load_array_str_by_float, r#"{ "test": 10 }"#, "test", load_array_str);
	not_found_test!(no_load_array_str_by_object, r#"{ "test": { "test": ["a", "b" ] } }"#, "test", load_array_str);
	not_found_test!(no_load_array_str_by_null, r#"{ "test": null }"#, "test", load_array_str);
	not_found_test!(no_load_array_str_partial_object, r#"{ "test": [3.0, { "key": "value" }] }"#, "test", load_array_str);
	not_found_test!(no_load_array_str_partial_array, r#"{ "test": [3.0, ["key"]] }"#, "test", load_array_str);
	not_found_test!(no_load_array_str_partial_null, r#"{ "test": [3.0, null] }"#, "test", load_array_str);

	not_found_test!(no_load_array_int64_by_string, r#"{ "test": "[a, b]" }"#, "test", load_array_int64);
	not_found_test!(no_load_array_int64_by_int, r#"{ "test": 192e-3 }"#, "test", load_array_int64);
	not_found_test!(no_load_array_int64_by_float, r#"{ "test": 10 }"#, "test", load_array_int64);
	not_found_test!(no_load_array_int64_by_object, r#"{ "test": { "test": ["a", "b" ] } }"#, "test", load_array_int64);
	not_found_test!(no_load_array_int64_by_null, r#"{ "test": null }"#, "test", load_array_int64);
	not_found_test!(no_load_array_int64_partial_string, r#"{ "test": [3, "nonsense"] }"#, "test", load_array_int64);
	not_found_test!(no_load_array_int64_partial_object, r#"{ "test": [3, { "key": "value" }] }"#, "test", load_array_int64);
	not_found_test!(no_load_array_int64_partial_array, r#"{ "test": [3, ["key"]] }"#, "test", load_array_int64);
	not_found_test!(no_load_array_int64_partial_null, r#"{ "test": [3, null] }"#, "test", load_array_int64);

	not_found_test!(no_load_array_f64_by_string, r#"{ "test": "[a, b]" }"#, "test", load_array_f64);
	not_found_test!(no_load_array_f64_by_int, r#"{ "test": 192e-3 }"#, "test", load_array_f64);
	not_found_test!(no_load_array_f64_by_float, r#"{ "test": 10 }"#, "test", load_array_f64);
	not_found_test!(no_load_array_f64_by_object, r#"{ "test": { "test": ["a", "b" ] } }"#, "test", load_array_f64);
	not_found_test!(no_load_array_f64_by_null, r#"{ "test": null }"#, "test", load_array_f64);
	not_found_test!(no_load_array_f64_partial_string, r#"{ "test": [3.0, "nonsense"] }"#, "test", load_array_f64);
	not_found_test!(no_load_array_f64_partial_object, r#"{ "test": [3.0, { "key": "value" }] }"#, "test", load_array_f64);
	not_found_test!(no_load_array_f64_partial_array, r#"{ "test": [3.0, ["key"]] }"#, "test", load_array_f64);
	not_found_test!(no_load_array_f64_partial_null, r#"{ "test": [3.0, null] }"#, "test", load_array_f64);
}

#[cfg(test)]
pub mod test_object {

	use super::Configuration;
	use super::JsonConfiguration;
	use super::rustc_serialize::json::Json;
	use std::collections::HashMap;
	use std::path::Path;
	use std::sync::RwLock;

	macro_rules! map {
	    [ $t:ident, $( $expr:expr ), * ] => (vec![$( $expr ), *].into_iter().map(|(a, b)| (a.to_string(), b)).collect::<HashMap<String, $t>>())
	}

	macro_rules! map_str {
	    [ $( $expr:expr ), * ] => (vec![$( $expr ), *].into_iter().map(|(a, b)| (a.to_string(), b.to_string())).collect::<HashMap<String, String>>())
	}

	macro_rules! map_i64 {
	    [ $( $expr:expr ), * ] => (map![i64, $($expr), *])
	}

	macro_rules! map_f64 {
	    [ $( $expr:expr ), * ] => (map![f64, $($expr), *])
	}

	value_test!(load_object_str_by_ref, r#"{ "test": { "a": "b" } }"#, "test",
		load_map_str, map_str![("a", "b")]);
	value_test!(load_object_str_by_string, r#"{ "test": { "a": "b" } }"#, &"test".to_string(),
		load_map_str, map_str![("a", "b")]);
	value_test!(load_object_str_by_path, r#"{ "test": { "value": { "a": "b" } } }"#, "test.value",
		load_map_str, map_str![("a", "b")]);
	value_test!(load_object_str_from_mixed, r#"{ "test": { "a1": "b", "a2": 10, "a3": 3.14 } }"#, "test",
		load_map_str, map_str![("a1", "b"), ("a2", "10"), ("a3", "3.14")]);

	value_test!(load_object_int64_by_ref, r#"{ "test": { "a": 2, "b": -1, "c": 3.0 } }"#, "test",
		load_map_int64, map_i64![("a", 2), ("b", -1), ("c", 3)]);
	value_test!(load_object_int64_by_string, r#"{ "test": { "a": 2, "b": -1, "c": 3.0 } }"#, &"test".to_string(),
		load_map_int64, map_i64![("a", 2), ("b", -1), ("c", 3)]);
	value_test!(load_object_int64_by_path, r#"{ "test": { "value": { "a": 2, "b": -1, "c": 3.0 } } }"#, "test.value",
		load_map_int64, map_i64![("a", 2), ("b", -1), ("c", 3)]);
	value_test!(load_object_int64_from_mixed, r#"{ "test": { "a1": -10.0, "a2": 10, "a3": 3.0 } }"#, "test",
		load_map_int64, map_i64![("a1", -10), ("a2", 10), ("a3", 3)]);

	value_test!(load_object_f64_by_ref, r#"{ "test": { "a": 2, "b": 3.14, "c": -5 } }"#, "test",
		load_map_f64, map_f64![("a", 2.0), ("b", 3.14), ("c", -5.0)]);
	value_test!(load_object_f64_by_string, r#"{ "test": { "a": 2, "b": 3.14, "c": -5 } }"#, &"test".to_string(),
		load_map_f64, map_f64![("a", 2.0), ("b", 3.14), ("c", -5.0)]);
	value_test!(load_object_f64_by_path, r#"{ "test": { "value": { "a": 2, "b": 3.14, "c": -5 } } }"#, "test.value",
		load_map_f64, map_f64![("a", 2.0), ("b", 3.14), ("c", -5.0)]);
	value_test!(load_object_f64_from_mixed, r#"{ "test": { "a1": -2, "a2": 10, "a3": 3.14 } }"#, "test",
		load_map_f64, map_f64![("a1", -2.0), ("a2", 10.0), ("a3", 3.14)]);

	not_found_test!(no_load_object_str_by_string, r#"{ "test": "[a, b]" }"#, "test", load_map_str);
	not_found_test!(no_load_object_str_by_int, r#"{ "test": 192e-3 }"#, "test", load_map_str);
	not_found_test!(no_load_object_str_by_float, r#"{ "test": 10 }"#, "test", load_map_str);
	not_found_test!(no_load_object_str_by_object, r#"{ "test": { "test": ["a", "b" ] } }"#, "test", load_map_str);
	not_found_test!(no_load_object_str_by_null, r#"{ "test": null }"#, "test", load_map_str);
	not_found_test!(no_load_object_str_partial_object, r#"{ "test": [3.0, { "key": "value" }] }"#, "test", load_map_str);
	not_found_test!(no_load_object_str_partial_array, r#"{ "test": [3.0, ["key"]] }"#, "test", load_map_str);
	not_found_test!(no_load_object_str_partial_null, r#"{ "test": [3.0, null] }"#, "test", load_map_str);

	not_found_test!(no_load_object_int64_by_string, r#"{ "test": "[a, b]" }"#, "test", load_map_int64);
	not_found_test!(no_load_object_int64_by_int, r#"{ "test": 192e-3 }"#, "test", load_map_int64);
	not_found_test!(no_load_object_int64_by_float, r#"{ "test": 10 }"#, "test", load_map_int64);
	not_found_test!(no_load_object_int64_by_object, r#"{ "test": { "test": ["a", "b" ] } }"#, "test", load_map_int64);
	not_found_test!(no_load_object_int64_by_null, r#"{ "test": null }"#, "test", load_map_int64);
	not_found_test!(no_load_object_int64_partial_string, r#"{ "test": [3, "nonsense"] }"#, "test", load_map_int64);
	not_found_test!(no_load_object_int64_partial_nonintegral_float, r#"{ "test": [3, 3.1] }"#, "test", load_map_int64);
	not_found_test!(no_load_object_int64_partial_object, r#"{ "test": [3, { "key": "value" }] }"#, "test", load_map_int64);
	not_found_test!(no_load_object_int64_partial_array, r#"{ "test": [3, ["key"]] }"#, "test", load_map_int64);
	not_found_test!(no_load_object_int64_partial_null, r#"{ "test": [3, null] }"#, "test", load_map_int64);

	not_found_test!(no_load_object_f64_by_string, r#"{ "test": "[a, b]" }"#, "test", load_map_f64);
	not_found_test!(no_load_object_f64_by_int, r#"{ "test": 192e-3 }"#, "test", load_map_f64);
	not_found_test!(no_load_object_f64_by_float, r#"{ "test": 10 }"#, "test", load_map_f64);
	not_found_test!(no_load_object_f64_by_object, r#"{ "test": { "test": ["a", "b" ] } }"#, "test", load_map_f64);
	not_found_test!(no_load_object_f64_by_null, r#"{ "test": null }"#, "test", load_map_f64);
	not_found_test!(no_load_object_f64_partial_string, r#"{ "test": [3.0, "nonsense"] }"#, "test", load_map_f64);
	not_found_test!(no_load_object_f64_partial_object, r#"{ "test": [3.0, { "key": "value" }] }"#, "test", load_map_f64);
	not_found_test!(no_load_object_f64_partial_array, r#"{ "test": [3.0, ["key"]] }"#, "test", load_map_f64);
	not_found_test!(no_load_object_f64_partial_null, r#"{ "test": [3.0, null] }"#, "test", load_map_f64);
}